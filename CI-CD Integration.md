NEMO CI/CD Integration steps
=

Throughout this guide, the [Intent-API](https://gitlab.eclipse.org/eclipse-research-labs/nemo-project/nemo-service-management/intent-based-sdk_api/intent-api#rabbitmq-integration) component will be used as guide. The manifests for the Intent-API can be found inside the Flux CD repository [here](https://gitlab.eclipse.org/eclipse-research-labs/nemo-project/nemo-infrastructure-management/flux-cd/-/tree/main/clusters/onelab-dev/nemo-svc/intent-api?ref_type=heads)

Dockerfile 
===

Every component needs a valid Dockerfile in order to deploy inside the NEMO Onelab Kubernetes Cluster.

NEMO Components container images shall be uploaded to the [docker.io](https://hub.docker.com/) instance under the account of [NEMO](https://hub.docker.com/u/nemometaos). (e.g. nemometaos/intent-api).

Building images (CI part)
===

Once a valid Dockerfile exists, the following ```.gitlab-ci.yml``` file must be created in the project root directory.


```yaml
include:
  - project: 'eclipsefdn/it/releng/gitlab-runner-service/gitlab-ci-templates'
    file: 'jobs/buildkit.gitlab-ci.yml'
  - project: 'eclipsefdn/it/releng/gitlab-runner-service/gitlab-ci-templates'
    file: 'pipeline-autodevops.gitlab-ci.yml'

stages:
  - build
  - test

variables:
  CI_REGISTRY_IMAGE: nemometaos/<component_name>

buildkit:
  extends: .buildkit
  
unit-test:
  stage: test
  script:
    - echo "Running unit tests... This will take about 10 seconds."
    # - docker run $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA /script/to/run/tests
    - sleep 10
    - echo "Tests passed succesfully!"
```

In the above example substitute the ```<component_name>``` with the name of your component. This configuration will do the following:
1. Upload the docker image ```nemometaos/<component_name>:latest``` tag every time a commit happens to the main branch
2. Upload the docker image ```nemometaos/<component_name>:<tag_name>``` tag every time you create a new tag.

For example, the creation of tag ```v0.1.1``` on the ```nemometaos/intent-api``` [here](https://gitlab.eclipse.org/eclipse-research-labs/nemo-project/nemo-service-management/intent-based-sdk_api/intent-api/-/tags/v0.1.1) uploaded to the docker registry the ```nemometaos/intent-api:v0.1.1``` image.

Important
===
Tags **MUST** be created in format ```vX.Y.Z``` where ```X,Y,Z``` are integer values and are always ascending.

> If the CI/CD pipeline is not triggering, Eclipse partner must be contacted in order to allow CI/CD in your gitlab projet

Deploying to kubernetes
===

The component that needs to integrate must provide all the kubernetes manifests and test that they can be deployed and work on the onelab cluster, by using the provided credentials to access the cluster.

In order to pull images from the ```nemometaos``` account, every ```namespace``` in kubernetes has the ```nemo-regcred``` secret that must be used as ```imagePullSecrets``` in the component ```Deployment``` manifest.

For example, as per the Intent-API:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: intent-api
  namespace: nemo-svc
  labels:
    app: nemo
    type: backend
    deployment: intent-api
spec:
  replicas: 3
  strategy:
    rollingUpdate:
      maxUnavailable: 1   # for pod anti-affinity to work
    type: RollingUpdate
  selector:
    matchLabels:
      pod: intent-api
  template:
    metadata:
      labels:
        app: nemo
        type: backend
        pod: intent-api
    spec:
      imagePullSecrets:
        - name: nemo-regcred
      containers:
        - name: django
          image: nemometaos/intent-api:v0.0.10
          imagePullPolicy: Always
...
```

Notice the 

```yaml
    spec:
      imagePullSecrets:
        - name: nemo-regcred
```

part.

DNS name, TLS termination
===
To reach your component from outside, you must create and ```Ingress``` object as follows:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: intent-api-ingress
  namespace: nemo-svc
  annotations:
    cert-manager.io/cluster-issuer: "letsencrypt-production"
spec:
  ingressClassName: "nginx"
  tls:
  - hosts:
    - intent-api.nemo.onelab.eu
    secretName: intent-api-tls
  rules:
  - host: intent-api.nemo.onelab.eu
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: intent-api
            port:
              number: 80
```

Service:
```yaml
          service:
            name: intent-api
            port:
              number: 80
```

must match the service attached to your Deployment manifest

```yaml
  annotations:
    cert-manager.io/cluster-issuer: "letsencrypt-production"
```

and 

```yaml
  tls:
  - hosts:
    - intent-api.nemo.onelab.eu
    secretName: intent-api-tls
```

automatically created certificates via ```cert-manager / Lets Encrypt```

```yaml
  ingressClassName: "nginx"
```

connects to the onelab nginx ingress controller

```yaml
- host: intent-api.nemo.onelab.eu
```

Your component must be <component-name>.nemo.onelab.eu.

The FQDN will be set by **Hassane Rahich** (reachable inside Slack #infrastructure channel)

Automating image deployments (CD part)
===

Once the manifests are created and the component is verified that is working, the manifests must be transferred to the [FLUX CD](https://gitlab.eclipse.org/eclipse-research-labs/nemo-project/nemo-infrastructure-management/flux-cd) repository.

The FLUX CD repository is structured in folders as follows:

```<cluster_name> / <kubernetes_namespace> / <component_name> / <component_subcomponents>```

Each component must be deployed into the respective folder that matched their kubernetes assigned namespace.

Sub dependencies (e.g. postgres, redis etc) can be also setup as **helm charts**.

Example redis & postgres helm chart dependency for the Intent-API can be found [here](https://gitlab.eclipse.org/eclipse-research-labs/nemo-project/nemo-infrastructure-management/flux-cd/-/tree/main/clusters/onelab-dev/nemo-svc?ref_type=heads).

WARNING!
====

Anything committed or uncommitted inside the FLUX CD repository will be reflected inside the onelab cluster.
For example, if a kubernetes manifest is changed manually inside the cluster, after a small amount of time, the change will be **reverted** to match what is actually inside the repository.

> It is highly advisable that commits to this repository are done as merge requests and/or through [me](https://gitlab.eclipse.org/atomaras) (I can be reached inside the NEMO Slack Channel)

More information about FLUX CD can be found [here](https://fluxcd.io/flux/)

Image updates
===

To automate the process, an ImageRepository & ImagePolicy CRD must be commited alongside the component manifests.
Example:


ImageRepository
```yaml
---
apiVersion: image.toolkit.fluxcd.io/v1beta1
kind: ImageRepository
metadata:
  name: intent-api
  namespace: flux-system
spec:
  image: nemometaos/intent-api
  interval: 1m0s
  secretRef:
    name: nemo-regcred

```

ImagePolicy
```yaml
---
apiVersion: image.toolkit.fluxcd.io/v1beta1
kind: ImagePolicy
metadata:
  name: intent-api
  namespace: flux-system
spec:
  imageRepositoryRef:
    name: intent-api
  filterTags:
    extract: $version
    pattern: ^(?P<version>v?\d+\.\d+.\d+[a-zA-Z]*)$
  policy:
    semver:
      range: '*'

```

The proper names must be set, those must reside inside the ```flux-system``` namespace, and the appropriate ```spec.image``` repository must be set.

After that, the Deployment manifest of the component must upsert the following to the line that defines the container image:
Example:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: intent-api
  namespace: nemo-svc
  labels:
    app: nemo
    type: backend
    deployment: intent-api
spec:
  replicas: 3
  strategy:
    rollingUpdate:
      maxUnavailable: 1 # for pod anti-affinity to work
    type: RollingUpdate
  selector:
    matchLabels:
      pod: intent-api
  template:
    metadata:
      labels:
        app: nemo
        type: backend
        pod: intent-api
    spec:
      imagePullSecrets:
        - name: nemo-regcred
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchExpressions:
                  - key: pod
                    operator: In
                    values:
                      - intent-api
              topologyKey: "kubernetes.io/hostname"
      containers:
        - name: django
          image: nemometaos/intent-api:v0.1.1 # {"$imagepolicy": "flux-system:intent-api"}
...
```

Notice the comment in line:

```yaml
          image: nemometaos/intent-api:v0.1.1 # {"$imagepolicy": "flux-system:intent-api"}
```

After that, any ***ascending, new tag version***, that is created from the component repository, will be:

1. Pushed to the docker hub repository
2. Set inside the Deployment manifest (version bump) as commit to the repository by Flux (example bot [commit](https://gitlab.eclipse.org/eclipse-research-labs/nemo-project/nemo-infrastructure-management/flux-cd/-/commit/be283d9fd9f3753533d9f271240d7d9ad77c9ee1))