

| **Group** | **Subgroup L1** | **Subgroup L2** | **Type** | **Responsible** |
| --- | --- | --- | --- | --- |
| NEMO Infrastructure Management | Federated Meta-Network Cluster Controller || group | TID |
| NEMO Infrastructure Management |5G enhancements | | group | CMC |
| NEMO Kernel | Meta-Orchestrator | | group | ATOS |
| NEMO Kernel | Intent-based Migration Controller | | group | ATOS |
| NEMO Kernel | Cybersecure Microservices' Digital Twin |  | group | ENG |
| NEMO Kernel | Secure Execution Environment | | group | RWTH |
| NEMO Service Management | Plugin & Applications Lifecycle Manager | | group | AEGIS |
| NEMO Service Management |Monetization and Consensus-based Accountability | | group | MAG |
| NEMO Service Management |Intent-based SDK/API | | group | SYN |
| NEMO Federated MLOps | Cybersecure Reinforcement Learning | Federated Learning | group | TSG|
| NEMO Federated MLOps | Cybersecure Reinforcement Learning | Reinforcement Learning|group|UPM|
| NEMO Federated MLOps | Gossip Learning | | group | SU |
| NEMO Federated MLOps | Federated Learning attacks | GAN data generation | group | INTRA |
| NEMO Federated MLOps | Federated Learning attacks | Attack detection | group | INTRA |
| NEMO PRESS & Policy Enforcement || | group | INTRA |
| NEMO Cybersecurity & Unified/ Federated Access Control | Identity Management | | group | SIMAVI |
| NEMO Cybersecurity & Unified/ Federated Access Control |Access Control | | group | SYN |
| NEMO Cybersecurity & Unified/ Federated Access Control |Secure Intercommunication | | group | STS |