# NEMO Keycloak and Postgresql demo project

## Docker Compose Configuration

1. `docker-compose.yml`: This file contains the Docker Compose configuration details for setting up Keycloak and PostgreSQL.

## Getting Started

Follow these steps to set up the project:

1. Clone the repository to your local machine:

   ```bash
   git clone <repository_url>
   ```

2. Navigate to the project directory:

   ```bash
   cd <project_directory>
   ```

3. Create a `.env` file and set the necessary environment variables. You can use the provided example below:

   ```env
   # .env

   # Keycloak Admin Credentials
   KEYCLOAK_VERSION=13.0.0
   PORT_KEYCLOAK=8080
   POSTGRESQL_USER=keycloak
   POSTGRESQL_PASS=keycloak
   POSTGRESQL_DB=keycloak 
   ```

4. Build and start the containers using Docker Compose:

   ```bash
   docker-compose up -d
   ```

## Initial Keycloak Admin Account Setup

To be executed the first time after the initial Keycloak installation for creating the admin account, follow these steps:

```bash
docker exec local_keycloak \
    /opt/jboss/keycloak/bin/add-user-keycloak.sh \
    -u $KEYCLOAK_ADMIN_USERNAME \
    -p $KEYCLOAK_ADMIN_PASSWORD \
&& docker restart local_keycloak
```

Make sure to replace `$KEYCLOAK_ADMIN_USERNAME` and `$KEYCLOAK_ADMIN_PASSWORD` with the values you set in the `.env` file.

Now, your Keycloak admin account is created, and the Keycloak container is restarted with the new credentials.

## Usage

After completing the setup steps, you can access Keycloak using your admin credentials:

- **Keycloak URL**: [http://localhost:28080/auth](http://localhost:28080/auth)
- **Admin Username**: admin (or the username you specified in the `.env` file)
- **Admin Password**: admin (or the password you specified in the `.env` file)

Feel free to customize the project further as needed for your specific use case.
```

This README provides an overview of setting up Keycloak and PostgreSQL using Docker Compose and how to create the initial admin account.
